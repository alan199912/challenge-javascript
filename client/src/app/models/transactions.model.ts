export class Transaction {

    constructor(_id = '', concept = '', budget = '', type = '', _user_id='') {
        this._id = _id;
        this.concept = concept;
        this.budget = budget;
        this.type = type;
        this._user_id = _user_id;
    }
    
    _id: string;
    concept: string;
    budget: string;
    type: string;
    _user_id: string;
}