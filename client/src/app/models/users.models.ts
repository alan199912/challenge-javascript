export class User {

    constructor(_id = '', username = '', password = '', email = '', fullname = '', role = '') {
        this._id = _id;
        this.username = username;
        this.password = password;
        this.email = email;
        this.fullname = fullname;
        this.role = role;
    }
    
    _id: string;
    username: string;
    password: string;
    email: string;
    fullname : string;
    role: string;

}