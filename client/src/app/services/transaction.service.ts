import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Transaction } from "../models/transactions.model";
import { User } from '../models/users.models';

@Injectable({
  providedIn: 'root'
})
export class TransactionService {

  selectedTransaction: Transaction;
  transactions: Transaction[];
  selectedUser: User;
  users: User[];
  readonly URL_API = 'http://localhost:3000/api/transactions';
  readonly URL_API_USERS = 'http://localhost:3000/api/users';

  constructor(private http: HttpClient) {
    this.selectedTransaction = new Transaction();
  }

  getUser() {
    return this.http.get(this.URL_API);
  }

  getTransactions() {
    return this.http.get(this.URL_API);
  }

  doTransactions(user: User, transaction: Transaction) {
    return this.http.post(this.URL_API + `/${user.username}`, transaction);
  }

}
