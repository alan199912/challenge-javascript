import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { User } from '../models/users.models';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  selectedUser: User;
  users: User[];
  readonly URL_API = 'http://localhost:3000/api/users';

  constructor(private http: HttpClient) {
    this.selectedUser = new User();
  }

  getUser() {
    return this.http.get(this.URL_API);
  }

  registerUser(user: User) {
    return this.http.post(this.URL_API, user);
  }

  loginUser(user: User) {
    return this.http.get(this.URL_API + `/${user.username}`);
  }

}
