import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Transaction } from 'src/app/models/transactions.model';
import { User } from 'src/app/models/users.models';
import { TransactionService } from 'src/app/services/transaction.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  usersGet: any ;
  user: any ;
  getTransactions: any ;
  getTransaction: any ;

  constructor( private _aRoute: ActivatedRoute,
              public userService: UserService,
              public transactionService: TransactionService ) {

    this._aRoute.params
          .subscribe( params => {

      this.userService.getUser()
            .subscribe(data => {

              this.usersGet = data;
              
              this.usersGet.forEach(elem => {
                if(elem.username == params.username) {
                  this.transactionService.getTransactions()
                    .subscribe( info => {
                      this.getTransactions = info;
                      this.getTransactions.forEach( note => {
                        if(elem.id == note.user_id) {
                          this.getTransaction = note;
                          console.log(note);
                        }
                      });  
                      
                    });
                  this.user = elem;
                  console.log(elem)
          }
        });
      });
    });

    


    console.log("HomeComponent -> this.getTransaction", this.getTransaction)

  }
  

  ngOnInit(): void {

  }

}
