import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/users.models';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  // session: boolean = false;
  user: any;

  constructor(public userService: UserService) { }

  ngOnInit(): void {

  }

  getUsers() {
    this.userService.getUser()
      .subscribe(res => {
        this.userService.users = res as User[];
        console.log(res);
      });
  }

}
