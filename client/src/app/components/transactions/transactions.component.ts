import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Transaction } from 'src/app/models/transactions.model';
import { User } from 'src/app/models/users.models';
import { TransactionService } from 'src/app/services/transaction.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.css']
})
export class TransactionsComponent implements OnInit {

  transaction: any;
  user: any;
  usersGet: any;

  constructor(public transactionService : TransactionService,
              public userService: UserService,
              private _aRoute: ActivatedRoute) { }

  ngOnInit(): void {

  }

  carryOutTransaction(form : NgForm) {

    this._aRoute.params.subscribe( params => {

      this.userService.getUser().subscribe( data => {
        this.usersGet = data;

        this.usersGet.forEach( elem => {

          if( elem.username === params.username ) {
            this.user = elem;
            console.log(this.user)

            this.transactionService.doTransactions(elem, form.value)
                .subscribe( data => {
            this.transaction = data;

            console.log("data ",this.transaction);
          }) 

          }
        });
      })
    });
    
    console.log("form ",form.value);
    
  }

  getTransaction() {
    this.transactionService.getTransactions()
        .subscribe( data => {
          this.transactionService.transactions = data as Transaction[];
          console.log(data)
        });
  }

}
