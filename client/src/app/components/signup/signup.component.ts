import { Component, OnInit } from '@angular/core';
import { NgForm } from "@angular/forms";
import { User } from "src/app/models/users.models";
import { UserService } from "../../services/user.service";


@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {


  constructor(public userService: UserService) { }

  ngOnInit(): void {

  }

  addUser(form: NgForm) {
    this.userService.registerUser(form.value)
      .subscribe(data => {
        this.resetForm(form);
        this.getUsers();
        console.log(data);
      });
  }

  getUsers() {
    this.userService.getUser()
      .subscribe(data => {
        this.userService.users = data as User[];
        console.log(data);
      });
  }


  resetForm(form?: NgForm) {
    if(form) {
      form.reset();
      this.userService.selectedUser = new User();
    }
  }

}

