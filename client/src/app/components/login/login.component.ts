import { Component, Input, OnInit } from '@angular/core';
import { NgForm } from "@angular/forms";
import { User } from "src/app/models/users.models";
import { UserService } from "../../services/user.service";
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
 
  user :any;
  

  constructor(public userService: UserService,
    private _router: Router) { }

  ngOnInit(): void {
  }

  logUser(form: NgForm) {
    
    this.userService.loginUser(form.value)
      .subscribe(data => {
        
        this.user = data;
        console.log(this.user) // undefined
        this.user.forEach(elem => {
          console.log("elem ",elem)
            this._router.navigate([`/home/${elem.username}`]);
            this.resetForm(form);
        });

      });
  }

  resetForm(form?: NgForm) {
    if(form) {
      form.reset();
      this.userService.selectedUser = new User();
    }
  }

}
